/* simple_html.c -- Create a minimally configured XmHTML widget */
#include <XmHTML/XmHTML.h>

int
main(int argc, char **argv)
{
	Widget toplevel;
	XtAppContext app;

	toplevel = XtVaAppInitialize(&app, "Demos", NULL, 0,
		&argc, argv, NULL, NULL);

	XtVaCreateManagedWidget("html", xmHTMLWidgetClass, toplevel,
		XmNvalue, "<html><body>A minimally configured XmHTML widget."
			"</body></html>",
		NULL);

	XtRealizeWidget(toplevel);
	XtAppMainLoop(app);
	return(0);
}
