/*****
* tka.h : XmHTML Toolkit Abstraction Public Interface, Gtk version.
*
* This file Version	$Revision: 1.1 $
*
* Creation date:		Mon Sep 28 08:49:25 CEST 1998
* Last modification: 	$Date$
* By:					$Author$
* Current State:		$State$
*
* Author:				XmHTML Developers Account
*
* Copyright (C) 1994-1998 by Ripley Software Development 
* All Rights Reserved
*
* This file is part of no particular project.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU  General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
* You should have received a copy of the GNU  General Public
* License along with this program; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*****/
/*****
* $Source$
*****/
/*****
* ChangeLog 
* $Log$
*
*****/ 

#ifndef _tka_h_
#define _tka_h_

#ifndef NeedFunctionPrototypes
#ifdef __STDC__
#define NeedFunctionPrototypes 1
#endif /* __STDC__ */
#endif /* NeedFunctionPrototypes */

/*****
* Toolkit independent rendering functions. This enables us to use the
* same engine for rendering to a display, text or postscript.
* 
* This abstraction makes it a *lot* easier when porting XmHTML to other
* toolkits, provided the display functions know how to deal/convert the
* X-specific types.
*****/
#define GC_FILL_SOLID				0
#define GC_FILL_TILED				1
#define GC_FILL_STIPPLED			2
#define GC_FILL_OPAQUE_STIPPLED		3

#define GC_CAP_NOT_LAST				0
#define GC_CAP_BUTT					1
#define GC_CAP_ROUND				2
#define GC_CAP_PROJECTING			3

#define GC_LINE_SOLID				0
#define GC_LINE_ON_OFF_DASH			1
#define GC_LINE_DOUBLE_DASH			2

#define GC_JOIN_MITER				0
#define GC_JOIN_ROUND				1
#define GC_JOIN_BEVEL				2

#define GC_GXcopy					0

#define GC_COORDMODE_ORIGIN			0
#define GC_COORDMODE_PREVIOUS		1

typedef struct _ToolkitAbstraction{
	void *dpy;					/* unused by gtk/XmHTML		*/
	GdkDrawable *win;			/* render area				*/
	Drawable defaultRoot;		/* this is an X drawable, not a gtk drawable */

	/*****
	* Screen definitions
	*****/
	gint width;					/* width in pixels			*/
	gint height;				/* height in pixels			*/
	gint widthMM;				/* width in millimeters		*/
	gint heightMM;				/* height in millimeters	*/

	/**********
	* Xlib function wrappers
	**********/

	/*****
	* GC properties
	*****/

	gint fill_style[4];
	gint cap_style[4];
	gint line_style[3];
	gint join_style[3];
	gint gc_func[2];
	gint coord_mode[2];

	/*****
	* GC functions
	*****/
	GdkGC*   (*CreateGC)( 
#ifdef NeedFunctionPrototypes
		struct _ToolkitAbstraction* 		/* tka */,
		GdkWindow*				/* d */,
		gulong					/* valuemask */,
		GdkGCValues*				/* values */
#endif
	);

	gint (*FreeGC)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*                  /* gc */
#endif
	);

	gint (*CopyGC)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*                  /* src */,
		gulong					/* valuemask */,
		GdkGC*                  /* dest */
#endif
	);

	gint (*SetFunction)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		gint					/* function */
#endif
	);

	gint (*SetClipOriginAndMask)(
#ifdef NeedFunctionPrototypes
		struct _ToolkitAbstraction* 		/* tka */,
		GdkGC*					/* gc */,
		gint					/* clip_x_origin */,
		gint					/* clip_y_origin */
		GdkPixmap*				/* pixmap */
#endif
	);

	gint (*SetTile)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		GdkPixmap*				/* tile */
#endif
	);

	gint (*SetTSOrigin)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		gint					/* ts_x_origin */,
		gint					/* ts_y_origin */
#endif
	);

	gint (*SetFillStyle)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		gint					/* fill_style */
#endif
	);

	gint (*SetFont)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		struct _XmHTMLFont*		/* font */
#endif
	);

	gint (*SetForeground)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		gulong					/* foreground */
#endif
	);

	gint (*SetBackground)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		gulong					/* background */
#endif
	);

	gint (*SetLineAttributes)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkGC*					/* gc */,
		guint					/* line_width */,
		gint					/* line_style */,
		gint					/* cap_style */,
		gint					/* join_style */
#endif
	);

	/*****
	* Font functions
	*****/

	XFontStruct* (*LoadQueryFont)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		const gchar*			/* name */
#endif
	);

	gint (*FreeFont)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkFont*				/* font_struct */
#endif
	);

	gint (*GetFontProperty)(
#ifdef NeedFunctionPrototypes
		GdkFont*				/* font_struct */,
		Atom					/* atom */,
		gulong*					/* value_return */
#endif
	);

	/*****
	* Cursor & pointer functions
	*****/
	gint (*UngrabPointer)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		Time					/* time */
#endif
	);

	gint (*DefineCursor)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkWindow*				/* w */,
		GdkCursor*				/* cursor */
#endif
	);

	gint (*UndefineCursor)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkWindow*				/* w */
#endif
	);

	gint (*FreeCursor)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkCursor*				/* cursor */
#endif
	);

	/*****
	* Color functions
	*****/

	gint (*ParseColor)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkColormap*			/* colormap */,
		const gchar*			/* spec */,
		GdkColor*				/* exact_def_return */
#endif
	);

	gint (*AllocColor)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkColormap*			/* colormap */,
		GdkColor*				/* screen_in_out */
#endif
	);

	gint (*QueryColor)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkColormap*			/* colormap */,
		GdkColor*				/* def_in_out */
#endif
	);

	gint (*QueryColors)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkColormap*			/* colormap */,
		GdkColor*				/* defs_in_out */,
		gint					/* ncolors */
#endif
	);

	gint (*FreeColors)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkColormap*			/* colormap */,
		gulong*					/* pixels */,
		gint					/* npixels */,
		gulong					/* planes */
#endif
	);

	/*****
	* Pixmap functions
	*****/

	GdkPixmap* (*CreatePixmap)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		guint					/* width */,
		guint					/* height */,
		guint					/* depth */
#endif
	);

	GdkPixmap* (*CreatePixmapFromBitmapData)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		gchar*					/* data */,
		guint					/* width */,
		guint					/* height */,
		gulong					/* fg */,
		gulong					/* bg */,
		guint					/* depth */
#endif
	);

	gint    (*FreePixmap)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkPixmap*				/* pixmap */
#endif
	);

	/*****
	* XImage functions
	*****/

	GdkImage *(*CreateImage)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkVisual*				/* visual */,
		guint					/* depth */,
		gint					/* format */,
		gint					/* offset */,
		gchar*					/* data */,
		guint					/* width */,
		guint					/* height */,
		gint					/* bitmap_pad */,
		gint					/* bytes_per_line */
#endif
	);

	void (*DestroyImage)(
#ifdef NeedFunctionPrototypes
		GdkImage *image			/* image */
#endif
	);

	gint  (*PutImage)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		GdkGC*					/* gc */,
		GdkImage*				/* image */,
		gint					/* src_x */,
		gint					/* src_y */,
		gint					/* dest_x */,
		gint					/* dest_y */,
		guint					/* width */,
		guint					/* height */
#endif
	);

	GdkImage (*GetImage)(
#if NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		gint					/* x */,
		gint					/* y */,
		guint					/* width */,
		guint					/* height */,
		gulong					/* plane_mask */,
		gint					/* format */
#endif
	);

	gulong (*GetPixel)(
#if NeedFunctionPrototypes
		GdkImage*				/* image */,
		gushort					/* x coordinate */,
		gushort					/* y coordinate */
#endif
	);
		
	/*****
	* string/text functions
	*****/

	gint (*TextWidth)(
#ifdef NeedFunctionPrototypes
		GdkFont*				/* font_struct */,
		const gchar*			/* string */,
		gint					/* count */
#endif
	);

	gint (*TextExtents)(
#ifdef NeedFunctionPrototypes
		GdkFont*				/* font_struct */,
		const gchar*			/* string */,
		gint					/* nchars */,
		gint*					/* direction_return */,
		gint*					/* font_ascent_return */,
		gint*					/* font_descent_return */,
		XCharStruct*			/* overall_return, no gdk equiv. */
#endif
	);

	/*****
	* Render functions
	*****/

	gint  (*DrawString)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		struct _XmHTMLFont*		/* font */,
		GdkGC*					/* gc */,
		gint					/* x */,
		gint					/* y */,
		const gchar*			/* string */,
		gint					/* length */
#endif
	);

	/*****
	* Render functions
	*****/

	gint (*DrawLine)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		GdkGC*					/* gc */,
		gint					/* x1 */,
		gint					/* x2 */,
		gint					/* y1 */,
		gint					/* y2 */
#endif
	);

	gint (*DrawLines)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		GdkGC*					/* gc */,
		GdkPoint*				/* points */,
		gint					/* npoints */,
		gint					/* mode */
#endif
	);

	gint (*DrawRectangle)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		GdkGC*					/* gc */,
		gint					/* x */,
		gint					/* y */,
		guint					/* width */,
		guint					/* height */
#endif
	);

	gint (*FillRectangle)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		GdkGC*					/* gc */,
		gint					/* x */,
		gint					/* y */,
		guint					/* width */,
		guint					/* height */
#endif
	);

	gint (*DrawArc)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		GdkGC*					/* gc */,
		gint					/* x */,
		gint					/* y */,
		guint					/* width */,
		guint					/* height */,
		gint					/* angle1 */,
		gint					/* angle2 */
#endif
	);

	gint (*FillArc)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* d */,
		GdkGC*					/* gc */,
		gint					/* x */,
		gint					/* y */,
		guint					/* width */,
		guint					/* height */,
		gint					/* angle1 */,
		gint					/* angle2 */
#endif
	);

	/*****
	* misc. functions
	*****/

	gint (*CopyArea)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* src */,
		GdkDrawable*			/* dest */,
		GdkGC*					/* gc */,
		gint					/* src_x */,
		gint					/* src_y */,
		guint					/* width */,
		guint					/* height */,
		gint					/* dest_x */,
		gint					/* dest_y */
#endif
	);

	gint (*ClearArea)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		GdkDrawable*			/* w */,
		gint					/* x */,
		gint					/* y */,
		guint					/* width */,
		guint					/* height */,
		Boolean					/* exposures */
#endif
	);

	gint (*Sync)(
#ifdef NeedFunctionPrototypes
		void*					/* display */,
		Boolean					/* discard */
#endif
	);

	/**********
	* X Toolkit Intrinsics wrappers
	**********/

	Boolean	(*IsRealized)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* widget */
#endif
	);

	Boolean (*IsManaged)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* rectobj */
#endif
	);

	void	(*ManageChild)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* child */
#endif
	);

	void (*UnmanageChild)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* child */
#endif
	);

	void	(*MoveWidget)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* widget */,
		gshort					/* x */,
		gshort					/* y */
#endif
	);

	void	(*ResizeWidget)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* widget */,
		gushort					/* width */,
		gushort					/* height */,
		gushort					/* border_width */
#endif
	);

	void	(*ConfigureWidget)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* widget */,
		gshort					/* x */,
		gshort					/* y */,
		gushort					/* width */,
		gushort					/* height */,
		gushort					/* border_width */
#endif
	);

	void    (*DestroyWidget)(
#ifdef NeedFunctionPrototypes
		GdkWidget*				/* widget */
#endif
	);

	void    (*SetMappedWhenManaged)(
#ifdef NeedFunctionPrototypes
		GtkWidget*				/* widget */,
		Boolean					/* mapped_when_managed */
#endif
	);

	void	(*RemoveTimeOut)(
#ifdef NeedFunctionPrototypes
		guint					/* timer */
#endif
	);

	guint	(*AddTimeOut)(
#ifdef NeedFunctionPrototypes
		void*					/* app_context */,
		guint32					/* interval */,
		GtkFunction				/* proc */,
		gpointer				/* closure */
#endif
	);

	/**********
	* Motif Wrappers
	**********/

	void	(*DrawShadows)(
#ifdef NeedFunctionPrototypes
		void *display,
		GdkDrawable* d,
		GdkGC* top_gc,
		GdkGC* bottom_gc,
#if NeedWidePrototypes
		gint x,
		gint y,
		gint width,
		gint height,
		gint shad_thick,
#else
		gshort x,
		gshort y,
		gushort width,
		gushort height,
		gushort shad_thick,
#endif /* NeedWidePrototypes */
		guint shad_type
#endif
	);

	/**********
	* Implementation Specific data
	**********/
	void *data;

	void (*FreeData)(
#ifdef NeedFunctionPrototypes
		void*
#endif
	);

}ToolkitAbstraction;

/* Create a new toolkit abstraction */
extern ToolkitAbstraction *XmHTMLTkaCreate(void);

/* destroy a toolkit abstraction */
extern void XmHTMLTkaDestroy(ToolkitAbstraction *tka);

/* Supply a new toolkit abstraction to a XmHTML Widget */
#ifdef NOTYET
extern Boolean XmHTMLTkaSet(GtkWidget *w, ToolkitAbstraction *tka);
#endif

/* Set the render area for a tka to use */
extern void XmHTMLTkaSetDrawable(ToolkitAbstraction *tka,
	GdkDrawable *drawable);

/* Set the display area for a tka to use */
extern void XmHTMLTkaSetDisplay(ToolkitAbstraction *tka, GtkWidget *w);

/* Recompute new top, bottom & highlight colors */
extern void XmHTMLTkaRecomputeColors(XmHTMLWidget html, Pixel bg_pixel);

/* Recompute highlight color */
extern void XmHTMLTkaRecomputeHighlightColor(XmHTMLWidget html,
	Pixel bg_pixel);

/* Recompute top & shadow colors */
extern void XmHTMLTkaRecomputeShadowColors(XmHTMLWidget html, Pixel base);

/* Don't add anything after this endif! */
#endif /* _tka_h_ */

