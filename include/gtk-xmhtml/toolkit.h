/*****
* toolkit.h : Gtk/XmHTML function & data type wrappers
*
* This file Version	$Revision: 1.2 $
*
* Creation date:		Thu Jan  8 04:32:19 GMT+0100 1998
* Last modification: 	$Date: 1998/04/27 07:03:52 $
* By:					$Author: newt $
* Current State:		$State: Exp $
*
* Author:				newt
*
* Copyright (C) 1994-1997 by Ripley Software Development 
* All Rights Reserved
*
* This file is part of the XmHTML Widget Library
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* About this file:
*
*	XmHTML is originally an Xt/Motif based Widget. To make porting to
*	other toolkits a bit easier, XmHTML uses a toolkit abstraction and a 
*	set of defines that allow you to replace all X/Xt/Xm functions used by
*	XmHTML.
*
*	All Xt/Xm functions are wrapped together in what I call a
*	ToolkitAbstraction (see tka.h for the definition, motif.c for
*	the Motif ToolkitAbstraction and gtk.c for the gtk ToolkitAbstraction).
*
*	There is one assumption though: that you never include a header containing
*	Xt or Xm specifics. If you do this however, you will need to override
*	a whole bunch of routines, typedefs and constants (don't worry, they
*	are all listed).
*
*****/
/*****
* $Source: /usr/local/rcs/Newt/XmHTML/RCS/toolkit.h,v $
*****/
/*****
* ChangeLog 
* $Log: toolkit.h,v $
* Revision 1.2  1998/04/27 07:03:52  newt
* more tka stuff
*
* Revision 1.1  1998/04/04 06:27:29  newt
* Initial Revision
*
*****/ 

#ifndef _toolkit_h_
#define _toolkit_h_

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

/* Required for at least font properties */
#include <X11/Xlib.h>
#include <X11/Xatom.h>

/* Private Header to be included for Gtk */
#define XmHTMLPrivateHeader "gtk-xmhtml-p.h"

/* No XCCP.h as our XCC adaption has been integrated in gdk :-) */
/* #define HAVE_XCCP_H */

/* No illegal xt access 'cause we're not using xt at all! */
#ifndef NO_XLIB_ILLEGAL_ACCESS
#define NO_XLIB_ILLEGAL_ACCESS 1
#endif

/*****
* Define if you want XmHTML not to use illegal access to gdk/gtk data
* structures
*****/
/* #undef NO_GTK_ILLEGAL_ACCESS */

/*****
* X/Xt types & constants for which gdk/gtk provides it's own implementation.
*****/

/* common types */
typedef GdkColor				XCOLOR;
typedef GdkColormap				*COLORMAP;
typedef GdkPixmap				*PIXMAP;
typedef GdkWindow				*WINDOW;
typedef GdkImage				XIMAGE;
typedef GdkFont					XFONTSTRUCT;
typedef GdkVisual				VISUAL;
typedef GdkGC					*XGC;
typedef GdkDrawable				*DRAWABLE;
typedef XCharStruct				XCHARSTRUCT;	/* no gdk equivalent */

typedef GdkEvent				XEVENT;
typedef GdkEventButton			XBUTTONPRESSEDEVENT;
typedef GdkEventButton			XBUTTONRELEASEDEVENT;

/* Color access macros */
#define GETP(c)		(c).pixel
#define GETR(C)		(c).red
#define GETG(C)		(c).green
#define GETB(C)		(c).blue

#define GETPP(c)	(c)->pixel
#define GETPR(c)	(c)->red
#define GETPG(c)	(c)->green
#define GETPB(c)	(c)->blue

/* byte ordering for this host */
#define LSBFIRST						GDK_LSB_FIRST
#define MSBFIRST						GDK_MSB_FIRST

/*****
* Provide (or override) a number of Xt typedefs
*****/
#ifdef _XtIntrinsic_h

/* Gtk applications should never include <X11/Intrinsic.h> */
#error Gtk/XmHTML may not include <X11/Intrinsic.h>.

#else

/*****
* typedefs for Xlib/Xt types used throughout XmHTML.
* Note that Pixel is also a typedef: it is not defined by Xlib but by
* the Intrinsics toolkit.
*****/
typedef GtkWidget		*Widget;
typedef GtkWidget		**WidgetList;
typedef gpointer		XtAppContext;
typedef GList			XtCallbackList;
typedef gint			XtIntervalId;
typedef gpointer		XtPointer;
typedef gchar			*String;
typedef guint			Cardinal;
typedef gushort			Dimension;
typedef gshort			Position;
typedef gulong			Pixel;

/* Straight from Intrinsic.h */
#ifdef CRAY
typedef glong			Boolean;
typedef glong			XtEnum;
#else
typedef gchar			Boolean;
typedef guchar			XtEnum;
#endif /* CRAY */

#endif

/*****
* X Function Wrappers
*****/

#define TkaCurrentTime GDK_CURRENT_TIME

/*****
* XFontStruct access macro's
*****/

#define TkaFont(XF)				(GDK_FONT_XFONT(XF))
#define TkaFontLeftBearing(XF)	((TkaFont(XF))->max_bounds.lbearing)
#define TkaFontRightBearing(XF)	((TkaFont(XF))->max_bounds.rbearing)
#define TkaFontWidth(XF)		((TkaFont(XF))->max_bounds.width)
#define TkaFontAscent(XF)		((TkaFont(XF))->ascent)
#define TkaFontDescent(XF)		((TkaFont(XF))->descent)
#define TkaFontMaxAscent(XF)	((TkaFont(XF))->max_bounds.ascent)
#define TkaFontMaxDescent(XF)	((TkaFont(XF))->max_bounds.descent)
#define TkaFontLineheight(XF)	(TkaFont(XF))->ascent + (TkaFont(XF))->descent)

/*****
* XImage wrappers & access macros.
*****/

#define TkaImageData(image) \
	(image->mem)

/* GdkImage has bytes per pixel, not bits per pixel */
#define TkaImageBitsPerPixel(image) \
	(image->bpp*8)

#define TkaImageBytesPerLine(image) \
	(image->bpl)

#define TkaImageByteOrder(image) \
	(image->byte_order)

#define TkaImageBitmapBitOrder(image) \
	(((GdkImagePrivate*)image)->ximage->bitmap_bit_order)

/*****
* check support for various combinations of bits per pixel
* FIXME:
* Gdk misses support for 2 and 4 bits per pixel.
*****/
#define TkaImageCheck2bpp(image) FALSE

#define TkaImageCheck4bpp(image) FALSE

#define TkaImageCheck24bpp(image) (image->bpp == 3)

#define TkaImageCheck32bpp(image) (image->bpp == 4)

/*****
* Xt Function wrappers
*****/

/* Check for the presence of a callback function */
#define TkaHasCallback(W,C) \
	gtk_xmhtml_signal_get_handlers (W, gtk_xmthml_signals[GTK_XMHTML_##C)

/* Activate a callback function */
#define TkaCallCallbackList(W,C,D) \
	gtk_signal_emit(GTK_OBJECT(W), gtk_xmhtml_signals[GTK_XMHTML_##C], D)

/* Set the position of a scrollbar slider */
#define TkaScrollbarSliderSetPosition(W,V) do{ \
	GtkAdjustment *adj = gtk_range_get_adjustment(GTK_RANGE((W))); \
	gtk_adjustment_set_value(adj, (gfloat)(V)); \
	}while(0)

/* XtName, XtClass */
#ifdef NO_GTK_ILLEGAL_ACCESS
#define TkaWidgetName(w)		gtk_widget_get_name(GTK_WIDGET(w))
#define TkaWidgetClassName(w)	gtk_type_name(GTK_WIDGET_TYPE(w))
#else
#define TkaWidgetName(w) ((w)->name == NULL ? "(unnamed)" : (w)->name)
#define TkaWidgetClassName(w) ((w)->object->klass->type)
#endif

#define XtAppWarning(CTX,MSG)	g_warning(MSG)
#define XtWarning(MSG)			g_warning(MSG)
#define XtAppError(CTX,MSG)		g_error(MSG)
#define XtError(MSG)			g_error(MSG)

/* gdk doesn't have the concept of Application Contexts */
#define XtWidgetToApplicationContext(widget) NULL

/*****
* Motif Wrappers
*****/
/*****
* In Motif, XmUpdateDisplay causes all events that are still left on the
* queue to be flushed *AND* to wait until all requests have been delivered.
* I guess an XFlush followed by an XSync will do the same (sort of).
*****/
#define XmUpdateDisplay(widget) do { \
	gdk_flush(); \
}while(0)

/*****
* Widget internal access wrappers
*****/
#define HTML_ATTR(field)	((XmHTMLWidget)html)->html.field
#define CORE_ATTR(field)	(GTK_WIDGET(html)->allocation).field
#define MGR_ATTR(field)		((XmHTMLWidget)html)->manager.field

/* subclass access methods */
#define ATTR_CORE(widget,field) \
	(GTK_WIDGET((widget))->allocation).(field)

#define ATTR_MGR(widget,field) \
	((XmHTMLWidget)widget)->manager.field

#define ATTR_HTML(widget,field) \
	((XmHTMLWidget)widget)->html.field

/* widely used subclass properties */

#define TkaGetColormap(widget) gtk_widget_get_colormap (GTK_WIDGET (widget))

#define TkaVisualGetDepth(widget) \
	ATTR_HTML(widget,xcc->visual->depth)

#define TkaVisualGetMapEntries(visual) \
	GDK_VISUAL_XVISUAL(visual)->map_entries

/*****
* XColorContext Wrappers
*****/

#define XCCCreate(w,v,c) \
	gdk_color_context_new (v, c)

#define XCCFree(c) \
	gdk_color_context_free (c)

#define XCCGetDepth(c) \
	(c)->visual->depth

#define XCCGetNumColors(c) \
	(c)->num_colors

#define XCCGetParentVisual(w) \
	gtk_widget_get_visual(w)

#define XCCGetPixels(cc,r,g,b,n,co,a) \
	gdk_color_context_get_pixels (cc,r,g,b,n,co,a)

#define XCCGetPixelsIncremental(cc,r,g,b,n,u,co,na) do{ \
	gdk_color_context_get_pixels_incremental (cc,r,g,b,n,u,co,na); \
}while (0)

#define XCCAddPalette(c,p,n) \
	gdk_color_context_add_palette (c,p,n)

#define XCCInitDither(cc) \
	gdk_color_context_init_dither (cc)

#define XCCGetIndexFromPalette(cc,r,g,b,f) \
	gdk_color_context_get_index_from_palette(cc,r,g,b,f)

#define XCCFreeDither(cc) \
	gdk_color_context_free_dither (cc)

typedef GdkColorContextDither XCCDither;

/* Don't add anything after this endif! */
#endif /* _toolkit_h_ */
