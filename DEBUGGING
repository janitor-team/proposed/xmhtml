This file: DEBUGGING

Table Of Contents
-----------------
1. Debug Resources

2. Adding Debug comments to XmHTML

3. _XmHTMLFullDebug

4. debug_menu.c

5. libXmHTML Compilation defines

6. Big Fat Warning

1. Debug Resources
------------------

XmHTML provides the following (undocumented) resources to aid in debugging.
These resources will always be available, but will only take effect when the
symbol DEBUG was defined during compilation.

Name						Type		Default		Access
XmNdebugDisableWarnings		Boolean		False		CSG
XmNdebugFilePrefix			String		NULL		CSG
XmNdebugLevels				String		NULL		CSG
XmNdebugEnableFullOutput	Boolean		False		CSG
XmNdebugSaveClipmasks		Boolean		False		CSG
XmNdebugNoAnimationLoopCount Boolean	False		CSG

XmNdebugDisableWarnings
	When set to False, XmHTML will show every warning that occurs. If disabled,
	a lot of warnings will be generated mainly by the parser and formatting
	routines.
	This resource has no effect when _XmHTMLWarning has been called with a
	NULL argument for the widget.

XmNdebugFilePrefix
	This resource enables you to take a look at the internal representation
	of the parsed HTML file, it need not be the same as the input file.
	This is mainly intended for checking the parser's ability to add closing
	elements on <p>, <li>, <dd> and <dt>.
	An output file is generated for every file/text parsed.
	The name of the output file is the prefix appended with the number of
	documents parsed so far, e.i., the first document will have the extension
	0, the next will have 1 and so on.

XmNdebugLevels
	This resource indicates the number of the source file of which you
	want to see debugging output. See below.

XmNdebugEnableFullOutput
	enable output from the _XmHTMLFullDebug routines.

XmNdebugSaveClipmasks
	will save all clipmasks XmHTML creates when encountering a transparent
	image. The clipmasks will be saved in a file called <image file>.xbm
	This option is not effective for progressive image loading.

XmNdebugNoAnimationLoopCount
	When set, all animations will loop forever; any loopcount specification in
	the NETSCAPE2.0 extension is ignored then.

When inside a debugger, two routines are available to examine the entire
list of XmHTMLObjectTable elements. They are:

dumpObjects(XmHTMLWidget html, String file);
	dumps object type, coordinates, line numbers on screen and in the input
	file and the pointer value for every object in the list.
	Dumps to file or stdout if file is NULL;

checkObjects(XmHTMLWidget html);
	checks for objects with zero coordinates and/or linenumbers.

2. Adding Debug comments to XmHTML
----------------------------------

The include file src/debug.h offers a way to add some sort of ``controlled''
debug statements in XmHTML. Each source file is assigned a number. A
command line option (-d) or the XmNdebugLevels resource controls from which 
source files debug output should be allowed. Debug output is only possible 
when DEBUG has been defined during compilation.

To add a debug statement, you use the following:

_XmHTMLDebug(<number>, (<text>));

The number is the source file number, and text is the text to output (printf
style). _XmHTMLDebug is the following macro:

#define _XmHTMLDebug(LEVEL,MSG) {\
	if(__rsd_debug_levels_defined[LEVEL] || __rds__debug_all == True) \
		{ __rsd_fprintf MSG; } \
}

xmhtml_debug_levels_defined is a static array with length MAX_DEBUG_LEVELS,
defined at 64 in debug.h.

__rsd_fprintf is an fprintf wrapper that writes the output either to stdout
or some other output file.

If you want to use it in an application, debug.h includes a routine which 
checks the command line options to your program in search of -d options:

extern void _XmHTMLSetDebugLevels(int *argc, char **argv);

This call will enable all given debug levels and remove any -d option from
the command line. The options recognized are:

-d[sequence of comma separated numbers]: enables debug output for the
	requested files;

-dall: enables debug output from *all* files;

-dfile:[filename]: causes all debug messages to be sent to file. Note
	that there may be *no* spacing between the color and the filename to use.
	The debug routines recognize a special filename: pid. If this is used,
	output will be send to a file called <process id>.out
	This is mainly intended for use in combination with -dfull: when this
	option is set, a *huge* amount of output is generated. You can also
	pipe the output to a file, but I think this is easier. 

-dfull: allow output from all _XmHTMLFullDebug() calls (see below).

To select a debug level, start an application with the -d command line option: 
<yourapp> -d1,4,5 selects debug output from source files 1, 4 and 5.

You can also set the XmNdebugLevels resource, either in an app-defaults file
or by means of the -xrm X11 command line option.

Combinations of numbers and words, or multiple words is not supported, although
multiple -d are allowed.

The levels 1 thru 48 are used by XmHTML

XmHTML uses the following source file mapping:

	Source file			Debug Id	Note
	-------------------------------------
	XmHTML.c			1
	format.c			2
	callbacks.c			3
	parse.c				4
	layout.c			5
	images.c/XmImage.c	6
	colors.c			7
	fonts.c				8
	XCC.c				9
	map.c				10
	frames.c			11
	forms.c				12
	quantize.c			13
	plc.c				14
	Image readers		15			All readXXX.c files
	paint.c				16
	table layout		17			subset of layout.c
	textsel.c			18
	reserved			19-48
	available			49-64

When you are running XmHTML with some or all debug levels defined, screen
updates will be slow or even incomplete. This goes away when running it
cleanly.

3. _XmHTMLFullDebug
-------------------

The debug routines also provide the macro _XmHTMLFullDebug(LEVEL,MSG).

Use of this macro is for heavily used functions (such as nearly every
event handler in XmHTML).

Use of this macro is the same as _XmHTMLDebug(), except that output is only
shown if -dfull was used (via command line or the XmNdebugEnableFullOuput
resource).

I added this macro to see what the library is doing in a certain
part of the program and a debugger was not handy to use. It's best to only 
enable this resource for tests with small documents, and only enable debug 
output from a single file, as it generates *HUGE* amounts of output.

4. debug_menu.c
---------------

The file src/debug_menu.c contains a routine that adds a XmHTML debug
menu to the menubar of your own application.

This menu allows you to select XmHTML debug output while an application is
running. The menu itself consists of togglebuttons, one for each source file
(or group of source files) that allow selectable debug output. It also
contains four buttons that toggle the following XmHTML debug resources:
XmNdebugDisableWarnings, XmNdebugEnableFullOutput, XmNdebugSaveClipmasks and
XmNdebugNoAnimationLoopCount.

Usage is as follows:

	_XmHTMLAddDebugMenu(Widget html, Widget menubar, String label)

where:
	html
		XmHTMLWidget id. Used to get & set the XmNdebug resources.
		It is a required value.
	menubar
		the Widget id of the menubar to which a debug menu should be
		added. It is a required value.
	label
		the menu label to use. When NULL, the string "Debug" is used.
		DebugAddMenu uses the first character of this label as the
		menu accelerator.

This function will set the values of each toggle button according to the
initially selected debug levels (the -d command line option) and the
initial value of any of the XmNdebug resources (hardcoded or through 
specification in a resource file).

The proto is defined in the debug_menu.h include file.

5. libXmHTML Compilation defines
--------------------------------

The following defines are present:

  - DEBUG
      debug.o and debug_menu.o are included in libXmHTML, all debug functions
      are fully available.

      When building applications, the debug functions are only active
      if you define DEBUG. You can safely intermix object files that have 
      haven't been compiled with -DDEBUG.

  - NDEBUG
      debug.o and debug_menu.o are *not* included in libXmHTML.a, none of the
      debug functions are available.

      Compiling your application with -DNDEBUG while using the XmHTML debug
      macros *will* lead to undefined linker errors.

6. Big Fat Warning
------------------

It is VERY dangerous to set breakpoints in any of XmHTML's event handlers
and action routines. This can lead to server locks.

Almost all event handlers are triggered by either keyboard or mouse actions.
The underlying code in X lib (or Intrinsics) momentarely seems to grab the
keyboard or the mouse pointer right before calling the event handler (or
action routine), and sometimes the grab is still present when the routine
is entered (it's *always* present when the library is running in synchronous
mode).

And then your server is locked. 

You can prevent pointer grabs when the focus policy of your window manager
doesn't follow the mouse and having the focus on the window where your
debug session is running. You can't prevent keyboard grabs.

The only way to solve this is to either log on via another system and kill
the debugger. If you can't log on you'll need to reboot.

