/*****
* GetRepTypes.c : lists all Representation Type Converters installed by Motif.
*
* This file Version	$Revision: 1.2 $
*
* Creation date:		Fri Nov 22 01:32:07 GMT+0100 1996
* Last modification: 	$Date: 1997/08/30 02:01:49 $
* By:					$Author: newt $
* Current State:		$State: Exp $
*
* Author:				newt
* (C)Copyright 1995-1996 Ripley Software Development
* All Rights Reserved
*
* Note: there is a call to XmRegisterConvertors in here. Motif 2.0.1 doesn't
*	have this call but it is _required_ under LessTif. It is probably also
*	required if you aren't running Motif 2.X
*****/
/*****
* ChangeLog 
* $Log: GetRepTypes.c,v $
* Revision 1.2  1997/08/30 02:01:49  newt
* ?
*
* Revision 1.1  1997/03/11 20:11:06  newt
* Initial Revision
*
*****/ 
#include <stdio.h>
#include <stdlib.h>
#include <Xm/RepType.h>

/*** External Function Prototype Declarations ***/

/*** Public Variable Declarations ***/

/*** Private Datatype Declarations ****/

/*** Private Function Prototype Declarations ****/

/*** Private Variable Declarations ***/

/*****
* Name:			main
* Return Type:	void
* Description:	main for GetRepTypes
* In:
*	argc:		no of command line arguments, unused
*	argv:		array of command line arguments, unused.
* Returns:
*	EXIT_SUCCESS
*****/
int
main(int argc, char **argv)
{
	XmRepTypeList rep_types;
	int i, j;

#if defined(LESSTIF) || XmVERSION < 2
	XmRegisterConverters();
#endif

	rep_types = XmRepTypeGetRegistered();

	printf("Representation Type Converters installed (Motif %i.%i.%i)\n",
		XmVERSION, XmREVISION, XmUPDATE_LEVEL);

	printf("\n-----------------\n");
	for(i = 0; rep_types[i].rep_type_name != NULL; i++)
	{
		printf("name: %s\n", rep_types[i].rep_type_name);
		printf("values:\n");
		for(j = 0; j < (int)rep_types[i].num_values; j++)
		{
			printf("\t%s", rep_types[i].value_names[j]);
			if(!((j+1) % 2))
				printf("\n");
		}
		printf("\n-----------------\n");
	}
	XtFree((char*)rep_types);
	exit(EXIT_SUCCESS);
}

