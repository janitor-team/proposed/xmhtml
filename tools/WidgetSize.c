/*****
* WidgetSize.c : computes total size of a raw XmHTMLWidget
*
* This file Version	$Revision: 1.3 $
*
* Creation date:		Tue Mar 11 06:06:35 GMT+0100 1997
* Last modification: 	$Date: 1997/10/23 00:30:36 $
* By:					$Author: newt $
* Current State:		$State: Exp $
*
* Author:				newt
*
* Copyright (C) 1994-1997 by Ripley Software Development 
* All Rights Reserved
*
* This file is part of the XmHTML Widget Library.
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Library General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*****/
/*****
* ChangeLog 
* $Log: WidgetSize.c,v $
* Revision 1.3  1997/10/23 00:30:36  newt
* XmHTML Beta 1.1.0 release
*
* Revision 1.2  1997/08/30 02:01:52  newt
* ?
*
* Revision 1.1  1997/03/11 20:11:10  newt
* Initial Revision
*
*****/ 
#include <stdio.h>
#include <stdlib.h>

#include <X11/Intrinsic.h>
#include <X11/IntrinsicP.h>
#include <X11/CoreP.h>
#include <X11/CompositeP.h>
#include <X11/ConstrainP.h>

#include <Xm/DrawingAP.h>
#include <Xm/ManagerP.h>
#include <Xm/PrimitiveP.h>
#include <Xm/ScrollBarP.h>
#include <XmHTML/XmHTMLP.h>

/*** External Function Prototype Declarations ***/

/*** Public Variable Declarations ***/

/*** Private Datatype Declarations ****/

/*** Private Function Prototype Declarations ****/

/*** Private Variable Declarations ***/

int
main(int argc, char **argv)
{
	int corePart_size, primitivePart_size, scrollbarPart_size;
	int htmlPart_size, managerPart_size, drawingPart_size;
	int scrollbar_size, drawing_size, html_size;
	int compositePart_size, constraintPart_size;

	corePart_size = sizeof(CorePart);
	compositePart_size = sizeof(CompositePart);
	constraintPart_size = sizeof(ConstraintPart);

	primitivePart_size = sizeof(XmPrimitivePart);
	managerPart_size = sizeof(XmManagerPart);
	scrollbarPart_size = sizeof(XmScrollBarPart);
	drawingPart_size = sizeof(XmDrawingAreaPart);
	htmlPart_size = sizeof(XmHTMLPart);

	scrollbar_size = corePart_size + managerPart_size + scrollbarPart_size;
	drawing_size = corePart_size + compositePart_size + constraintPart_size +
		managerPart_size + drawingPart_size;
	html_size = corePart_size + compositePart_size + constraintPart_size +
		managerPart_size + htmlPart_size;

	printf("XmHTML version : %i\n\n", XmHTMLVersion);
	printf("size of XmHTMLPart      : %i\n", htmlPart_size);
	printf("size of XmHTMLRec       : %i\n", html_size);
	printf("size of XmScrollBarRec  : %i\n", scrollbar_size);
	printf("size of XmDrawingAreaRec: %i\n", drawing_size);

	printf("Total size of XmHTMLWidget: %i\n",
		html_size + 2*scrollbar_size + drawing_size);
	exit(EXIT_SUCCESS);
}
