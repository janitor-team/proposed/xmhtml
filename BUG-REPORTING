[copied and apdapted from lesstif 0.75a]


                     REPORTING BUGS AND SUBMITTING FIXES
                     ===================================
                              January 30, 1997

Table of Contents
-----------------

  1. Introduction
  2. What you'll need to find bugs
  3. Recommended reading
  4. Isolating bugs
  5. X Protocol Errors
  6. Submitting bug reports
  7. Submitting patches
  8. Sample bug report


1. Introduction
---------------

    Bug hunting in XmHTML isn't really all that different from bug hunting
  in X application code, with a few notable exceptions.  We encourage users to
  help us make XmHTML a better product by doing some preliminary ground-work
  for the developers when you find a bug in the library.  Of course, what
  we really want is a patch that fixes the bug, but we'll settle for some
  help.

  Send your bugreports and patches to: ripley@xs4all.nl


2. What you'll need
-------------------

  Let's see:

   o - a computer (Ta-dah!)
   o - the X Window System, at release level R5 or above. It will probably
       also work under R4, but that must be verified.
   o - a compiler/assembler/linker
   o - a debugger
   o - some test code

  Of course, if you don't have that, you probably don't need to read this,
  do you?


3. Recommended Reading
----------------------

    The following list of books are nice to have, especially the Intrinsics
  and the Motif books, but you can probably get away without them if you
  have a full set of man pages.  One thing, though: if you want to understand
  what widget methods do, and how a widget works, they are an absolute
  necessity.

  The O'Reilly books "The Definitive Guide to the X Window System".  Of these,
  you'll want:
	Volume 1: Xlib Programming Manual
	Volume 2: Xlib Reference Manual
	Volume 4: X Toolkit Intrinsics Programming Manual
	Volume 5: X Toolkit Intrinsics Reference Manual
	Volume 6A: Motif Programming Manual
	Volumn 6B: Motif Reference Manual
    If you have R6 (Linux, *BSD, others), you'll want:
	Programmer's Supplement for Release 6

  From the OSF, you'll want
	OSF/Motif Programmer's Reference (for 1.2)
	OSF/Motif Programmer's Guide (for 1.2)
  The style guide is nice, but not required.


4. Isolating bugs
-----------------

    You found a bug in XmHTML, and you want to help.  Problems generally fall 
  into four broad categories:

    o - behavior
    o - visual
    o - X Protocol Error
    o - core dump

    One thing to keep in mind is that the location that the problem occurs in
  is not necessarily the same as the location that caused the problem; this is
  due to the (normally) asynchronous nature of X applications.  When your
  application has a problem, the first thing to do is run the application
  synchronously, to see if the location of the error changes (you'll be
  surprised how often it will).  Here's how to do it with gdb:

hobbes [212][18:39] [/home/newt/src/XmHTML-1.0.4/examples] >gdb example_2
GDB is free software and you are welcome to distribute copies of it
 under certain conditions; type "show copying" to see the conditions.
There is absolutely no warranty for GDB; type "show warranty" for details.
GDB 4.14 (i486-linux), Copyright 1995 Free Software Foundation, Inc...
(gdb) run -sync
Starting program: /home/newt/src/XmHTML-1.0.4/examples/./example_2 -sync

-----
    If you run the program with a -d switch, it'll probably spit out a bunch
  of warnings. Note that the commands passed to the run command are actually 
  command line arguments to the program.

  On some systems, you may find that you can't set breakpoints at the start
  if you use shared libraries.  That's because the symbols won't be resolved
  until a process image exists.  To work around this, set a breakpoint at
  main(), and run.  Once the breakpoint at main is reached, you'll find that
  you can set the other breakpoints.

    Once you've had your program dump core:), just say "where".  It will
  show a trace stack up to the point where the error occured (most likely
  a sigsegv). This will help you track down what the most probable cause of
  the error is.

5. About X Protocol Errors
--------------------------

  Sometimes a program will fail with something like:

X Error: Bad Drawable
  (other stuff)

  or something similar.  This is a problem with something that got sent to the
  X server.

    This isn't hard to track down if you've run in synchronous mode.  Just set
  a breakpoint at the exit() function, run with the -sync option, and you'll
  get a trace from the point where the error occurred, plus some other
  information.  These are crucial to proper XmHTML functionality -- these
  must be stamped out.

  X Protocol errors haven't been observed yet with XmHTML directly. It has
  occured when the parent of XmHTML is a gadget.


6. Submitting bug reports
-------------------------

    When (not if) you find a problem with XmHTML, we really want to know.
  Ideally, a patch is what we'd prefer.  Failing that, we need sample code,
  and a stack trace of the problem (even if it was an X Error).  If it was
  a behavioral or visual problem, just a description will do, although some
  code that exhibits the behavior would be nice.  Try to be complete if you
  send just a description -- meaning if you send something like:

  "Well, I ran an app, and the thingie at the bottom of the wossname didn't
   look right"

  Your are in all likelihood going to be ignored.

    We also need the version number of the file that had the problem.
  Please do not tell us the problem two months after you saw it, the code 
  base changes too rapidly for us to be able to make anything meaningful out 
  of a bug report that old. See section 8 for a sample bug report.

7. Submitting patches
---------------------

    Patches are the preferred form of bug report, as it lessens our efforts
  to track down where the bugs are.  If you do submit a patch, please try to
  keep a few rules in mind:

    o when writing your fix, please try to follow the coding style of the file
      you are fixing. 

    o try not to submit a fix that creates dead code.  Using "#if 0" or
      commented code is discouraged.  We can do that (because, after all, we
      are the elitist core team;), but you are discouraged from that, because
      it makes the patches hard to read.

    o do not be discouraged if your patch is not accepted verbatim.  Often
      people submit patches that one or more of the core team will recognize
      as identifying a problem, but the core member will recognize that your
      fix will break some other piece of functionality.  We'll try to tell
      you, but we're human, so we may forget.

    o Remember that relay mailers are free to alter the content of text
      messages, and often do so.  Lines should always be less than 80
      characters in length; if you are working on OS/2, remember that mailers
      will usually convert <carriage-return><line-feed> into just <line-feed>.
      This has in the past caused patches to be rejected by the patch program,
      and thus by us.

    o We prefer patches in the "unified diff" format (with GNU diff, this is
      "diff -u").  If you have to, you can send a "context diff".  Please
      use one of those two formats.


8. Sample Bug Report
--------------------

Shown below is a sample bug report. Feel free to add any extra comments
you wish.
 
    ========================================================================
    From  : <your name and email address>

    XmHTML: <XmHTML version, see XmHTMLVersion in XmHTML.h >
    Motif : <motif version, see XmVersion in Xm/Xm.h>
    X11   : <X11 version, see XtVersion in X11/IntrinsicP.h>

    system: <output from uname -a>

    File  : <XmHTML source file where bug is located, if applicable>

    Bug description:
        <a short description of the bug itself. If applicable, Attach a 
         trace stack to this report>

    Repeated by:
        <a short description on how to repeat this bug, if applicable>

    Proposed fix:
    	<a short description of a fix, or attach a patch to this report>
    ========================================================================

A sample:

    ========================================================================
    From  : Koen D'Hondt, ripley@xs4all.nl

    XmHTML: 1004
    Motif : 2000
    X11   : 11006

    system: Linux hobbes 2.0.26 #3 Wed Nov 27 18:16:53 GMT+0100 1996 i586

    File  : images.c, line 104, revision 1.2

    Bug description:
       a memory leak: the return value from _XmHTMLTagGetValue does not get
       freed.

    Repeated by:

    Proposed fix:
       add a free(chPtr)
    ========================================================================

Thanks, and remember: all help is appreciated.
