/*****
* ftypes.h : list of file associates. Used by example_2 when displaying
*			directory contents.
*
* This file Version	$Revision: 1.1 $
*
* Creation date:		Wed Sep  9 03:46:55 CEST 1998
* Last modification: 	$Date$
* By:					$Author$
* Current State:		$State$
*
* Author:				XmHTML Developers Account
*
* Copyright (C) 1994-1998 by Ripley Software Development 
* All Rights Reserved
*
* This file is part of no particular project.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU  General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*  General Public License for more details.
*
* You should have received a copy of the GNU  General Public
* License along with this program; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*****/
/*****
* $Source$
*****/
/*****
* ChangeLog 
* $Log$
*
*****/ 

#ifndef _ftypes_h_
#define _ftypes_h_

/*****
* The first three of these file types are viewable directly with XmHTML.
* The DIR and OPENDIR types are read and , processed and displayed via
* XmHTML.
* All other types require an external viewer.
*****/
#define FILE_TXT		0		/* plain text document, XmHTML		*/
#define FILE_HTML		1		/* HTML document, XmHTML			*/
#define FILE_IMG		2		/* image, XmHTML					*/
#define FILE_DIR		3		/* a directory, XmHTML				*/
#define FILE_OPENDIR	4		/* the current directory, XmHTML	*/
#define FILE_BIN		5		/* a binary file					*/
#define FILE_UNKNOWN	6		/* all other types					*/

typedef struct{
	String ext;			/* list of extensions, separated by a | */
	int type;			/* type of file	*/
	String icon;		/* name of icon to use */
	String descrip;		/* accompying description */
#if 0
	String mime_type;	/* mime type for this entry */
#endif
}fileType;

/*****
* Note on extensions: case is ignored when determining the type of a file!
*****/

static fileType FileTypes[] = {
	/* file types known to example_2 */
	{ ".c", FILE_TXT, "text.document", "C program text" },
	{ ".cc|.c++", FILE_TXT, "text.document", "C++ program text" },
	{ ".h", FILE_TXT, "text.document", "C/C++ header file" },

	/* archive types */
	{ ".tar.gz|.tgz", FILE_BIN, "archive", "Gzipped tar archive" },
	{ ".tar.z|.tz", FILE_BIN, "archive", "Compressed tar archive" },
	{ ".tar", FILE_BIN, "archive", "Tar archive" },

	{ ".rpm.gz", FILE_BIN, "archive",
		"Gzipped Redhat Package Manager archive" },
	{ ".rpm.z", FILE_BIN, "archive",
		 "Compressed Redhat Package Manager archive" },
	{ ".rpm", FILE_BIN, "archive",
		 "Redhat Package Manager archive" },

	{ ".deb.gz", FILE_BIN, "archive",
		"Gzipped Debian archive" },
	{ ".deb.z", FILE_BIN, "archive",
		 "Compressed Debian archive" },
	{ ".deb", FILE_BIN, "archive",
		 "Debian archive" },

	/* compressed files */
	{ ".gz|.gzip", FILE_BIN, "compressed.document", "Gzipped file" },
	{ ".z", FILE_BIN, "compressed.document", "Compressed file" },

	{ ".o", FILE_BIN, "binary.document", "Compiled object module" },

	/* image types */
	{ ".gif", FILE_IMG, "image", "GIF image" },
	{ ".jpg|.jpeg", FILE_IMG, "image", "JPEG image" },
	{ ".png", FILE_IMG, "image", "PNG image" },
	{ ".xpm", FILE_IMG, "image", "X11 Pixmap image" },
	{ ".xbm", FILE_IMG, "image", "X11 Bitmap image" },

	/* document types */
	{ ".html|.htm", FILE_HTML, "html", "HTML document" },
	{ ".txt|.text", FILE_TXT, "text.document", "Plain text document"},
	{ ".doc", FILE_TXT, "document", "Other document"},

	/* directory types */
	{ "..", FILE_DIR, "previous", "Parent directory" },
	{ NULL, FILE_DIR, "folder", "Directory" },
	{ NULL, FILE_OPENDIR, "folder.open", "Current directory" },

	/* for all other documents */
	{ NULL, FILE_UNKNOWN, "unknown.document", "" },
};


/* Don't add anything after this endif! */
#endif /* _ftypes_h_ */

